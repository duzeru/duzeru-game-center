# DuZeru Game Center

Interface para emulador Mednafen com suporte à games de Sega Genesis e Super Nintendo

<img src="https://gitlab.com/duzeru/duzeru-game-center/wikis/uploads/8dff5959ffd352f0beec75b3a5ba171d/DuZeru_Game_Center.png">

<h3> Introdução </h3>
<p> Desde mil novecentos e sei lá quanto eu gostei de games de Mega Driver (Gênesis) e Super Nintendo. O tempo passou e os gráficos, consoles, jogabilidade e outros fatores evoluíram mas minha vontade nostálgica sempre veio para jogar estes games retrô.
Este Ano 2018, consegui catalogar os melhores jogos já lançados para estes dois consoles e juntei um um único lugar. Agora outros apaixonados como eu em Jogos retrô podem se divertir nas curtas horas vagas. </p>

<p> Caso não for programador e deseja ajudar na tradução, contamos com seu apoio. O arquivo está em: 
https://gitlab.com/duzeru/duzeru-game-center/tree/master/opt/duzeru-game-center/resources/app/locate
</p>

## Compilação

Crie um diretório
mkdir game-center_amd64

Adicione 
Para compilar basta apenas baixar todos os diretórios e executar o comando:
dpkg-deb -b <nome-do-diretorio>

<h3> Tecnologias adjacentes </h3>

<img src="https://gitlab.com/duzeru/duzeru-game-center/wikis/uploads/e02476e237a123e029948cda072736f6/Electron_Wordmark.svg.png" width="230px" height="40px">

O Game Center é construído com tecnologias adjacentes. A primeira é <a href="https://electronjs.org/" target="_blank"> Electron </a>, é ele que faz a mágica acontecer quando escolhemos um jogo.


<img src="https://gitlab.com/duzeru/duzeru-game-center/wikis/uploads/273025914cb7c74d0e83d7a41fb01400/HTML5_Logo_512.png" width="75px" height="75px">

A segunda tecnologia é o <a href="https://www.w3schools.com/html/html5_intro.asp" target="_blank"> HTML5 </a>, que nos traz a interface bela, bem organizada com escolha do melhor game para se divertir.


<img src="https://gitlab.com/duzeru/duzeru-game-center/wikis/uploads/9ac78e7c5e23cca8bf1c6634ecad05ee/materialize.png" width="75px" height="75px">

Na terceira tecnologia utilizada está o <a href="https://www.w3schools.com/css/css_intro.asp" target="_blank"> HTML5 </a>. Os códigos em CSS não foram escritos "do zero", foi utilizado o <a href="https://materializecss.com/" target="_blank"> Materialize </a>. Uma estrutura de front-end responsiva moderna baseada em Material Design.

<h3> Motor de emulação </h3>

O Motor para emular os jogos é o <a href="https://mednafen.github.io/" target="_blank"> Mednafen </a>. Um emulador portátil, utilizando OpenGL e SDL, baseado em argumentos (linha de comando) e multi-sistemas. O Mednafen tem a capacidade de remapear as funções das teclas de atalho e as entradas do sistema virtual para um teclado, um joystick ou ambos simultaneamente. Salvar estados são suportados, assim como o rebobinamento de jogos em tempo real.


<h3> Categorias </h3>
Ação
Corrida
Luta
Aventura
Esporte

<h3> Jogos </h3>

<h5>SEGA</h5>
<table style="width:100%">
  <tr>
    <th>Jogo</th>
    <th>Categoria</th> 
  </tr>
  <tr>
    <td>Comics Zone</td>
    <td>Ação</td>
  </tr>
  <tr>
    <td>Kid Chameleon</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Sonic The Hedgehog 3</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Vectorman 2</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Vectorman</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Alladin</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Donkey Kong 99</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Mega Bomberman</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Moonwalker Michael</td>
    <td>Aventura</td>
  </tr>
  <tr>
    <td>Streets Of Range 3</td>
    <td>Ação</td>
  </tr>
  <tr>
    <td>Streets Of Range 2</td>
    <td>Ação</td>
  </tr>
  <tr>
    <td>X-Mem 2</td>
    <td>Ação</td>
  </tr>
  <tr>
    <td>Mortal Kombat 3</td>
    <td>Luta</td>
  </tr>
  <tr>
    <td>Mortal Kombat 5</td>
    <td>Luta</td>
  </tr>
  <tr>
    <td>Dragon Bruce Lee</td>
    <td>Luta</td>
  </tr>
  <tr>
    <td>Shinobi 3</td>
    <td>Ação</td>
  </tr>
  <tr>
    <td>Super Hang On</td>
    <td>Corrida</td>
  </tr>
  <tr>
    <td>Kawasaki Superbike</td>
    <td>Corrida</td>
  </tr>
  <tr>
    <td>Road Rash 3</td>
    <td>Corrida</td>
  </tr>
  <tr>
    <td>Road Rash 2</td>
    <td>Corrida</td>
  </tr>
  <tr>
    <td>Top Gear 2 </td>
    <td>Corrida</td>
  </tr>
  <tr>
    <td> Super Monaco GP </td>
    <td>Corrida</td>
  </tr>
  <tr>
    <td> Out Run </td>
    <td>Corrida</td>
  </tr>
  <tr>
    <td> Golden Axe III </td>
    <td>Ação</td>
  </tr>
  <tr>
    <td> Golden Axe II </td>
    <td>Ação</td>
  </tr>
  <tr>
    <td> Shaq Fu </td>
    <td>Ação</td>
  </tr>
  <tr>
    <td> Robocop Vs Terminator </td>
    <td>Ação</td>
  </tr>
  <tr>
    <td> Mutant Ninja Turtles </td>
    <td>Ação</td>
  </tr>
  <tr>
    <td> Altered Beast </td>
    <td>Ação</td>
  </tr>
  <tr>
    <td> Side Pocket </td>
    <td>Esporte</td>
  </tr>

</table>

<h5>Super Nitendo</h5>
<table style="width:100%">
  <tr>
    <th>Jogo</th>
    <th>Fategoria</th> 
  </tr>
  <tr>
    <td>Super Mario World</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>Donkey Kong</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>Super Mario Kart</td>
    <td>Corrida</td> 
  </tr>
  <tr>
    <td>Ultimate Mortal Kombat</td>
    <td>Luta</td> 
  </tr>
  <tr>
    <td>Contra III</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>Final Fight III</td>
    <td>Ação</td> 
  </tr>
  <tr>
    <td>X-Men Mutant Apocalypse</td>
    <td>Ação</td> 
  </tr>
  <tr>
    <td>Marvel Super Heroes</td>
    <td>Ação</td> 
  </tr>
  <tr>
    <td>Side Pocket</td>
    <td>Esporte</td> 
  </tr>
  <tr>
    <td>Mickey Mania</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>Street Fighter</td>
    <td>Luta</td> 
  </tr>
  <tr>
    <td>Mega Man 7</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>Spider-Man Separation</td>
    <td>Ação</td> 
  </tr>
  <tr>
    <td>Doom Troopers</td>
    <td>Ação</td> 
  </tr>
  <tr>
    <td>Legend Of Zelda</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>Sonic Blast Man 2</td>
    <td>Ação</td> 
  </tr>
  <tr>
    <td>Ms. Pac Man</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>The Mask</td>
    <td>Aventura</td> 
  </tr>
  <tr>
    <td>Super Soccer</td>
    <td>Esporte</td> 
  </tr>
</table>

<h5> Viu algum erro? ajude e seus créditos de contribuição será preservado. </h5>

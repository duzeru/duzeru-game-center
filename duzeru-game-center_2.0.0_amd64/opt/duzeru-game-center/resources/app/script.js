const remote = require('electron').remote
const main = remote.require('./mainElectron.js');
const shell = require('shelljs')


$(document).ready(function () {

    $('#search').focus();
    btnClear();


    /*input search*/
    $('#search').keyup(function () {
        var nomeFiltro = $(this).val().toLowerCase();
        $('.icons').find('div').each(function () {
            var corresponde = $(this).text().toLowerCase().indexOf(nomeFiltro) >= 0;
            $(this).parent().parent().css('display', corresponde ? '' : 'none');
        });
    });

    setTimeout(function () {

    }, 200);



});


var modalConf = (function () {
    var pnLocal;
    var advancedConfig;
    function modal() {
        $('#pnConf').mofo({
            noTitleDisplay: true,
            modal: false,
            fullScreen: true,
            buttons: {
                Btn: {
                    html: '<i class="fas fa-arrow-circle-left" aria-hidden="true"></i> ' + locate.back,
                    style: 'float:left',
                    click: function () {
                        $('#pnConf').mofo('close');
                    },
                },
                AdvanceConfig: {
                    html: '<i class="fas fa-cog" aria-hidden="true"></i> ' + locate.AdvancedConfig,
                    click: function () {
                        shell.exec(advancedConfig, () => {
                        });
                    },
                }
            },
            open: function () {
                $('#pnConf').find('.mofo-modal-content').html($('#' + pnLocal).html());

            },
            close: function () {
                $('.pnInf').show();
                $('#listIcon').remove();
                $('#pnListIcon').remove();
            }
        });
        $('#pnConf').css('z-index', '785');
    }

    $('.icons').click(function () {

        pnLocal = $(this).attr('pn');
        advancedConfig = $(this).attr('conf');

        var position = $(this).find('img').position();
        var html = $(this).find('div').html();
        position.width = $(this).find('img').attr('width');
        var src = $(this).find('img').attr('src');
        var img = $('<img>', {
            id: 'listIcon',
            css: position,
            src: src
        });
//        var div = $('<div>', {html: html, id: 'pnListIcon'});
//        $('.pnBarra').prepend(div).prepend(img);
//        $('.pnInf').hide();
//        setTimeout(function () {
//            $('#listIcon').css({
//                top: -5,
//                left: 10,
//                width: 65
//            });
//            modal();
//        }, 80);
    });
})();

function btnClear() {
    function tog(v) {
        return v ? 'addClass' : 'removeClass';
    }
    $(document).on('input', '.btnClear', function () {
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function (e) {
        $(this)[tog(this.offsetWidth - 30 < e.clientX - this.getBoundingClientRect().left)]('onX');
    }).on('touchstart click', '.onX', function (ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();
        $('.icons').parent().css('display', '');
    });
}

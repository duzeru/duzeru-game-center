//http://www.metamodpro.com/browser-language-codes


var idioma = (navigator.browserLanguage != undefined) ? navigator.browserLanguage : navigator.language;
getLocate(idioma.toLowerCase());


function getLocate(id) {
    $.getScript('locate/' + id)
            .done(function () {
                setLocate();
            })
            .fail(function () {
                getLocate('en');
            });
}


function setLocate() {
    $("[idiom]").each(function () {
        var strTr = locate[$(this).attr('idiom')];
        $(this).html(strTr);
    });

}

apt-get install zlib1g-dev
gcc
clang
libsndfile1
libsndfile-dev
libsdl2-dev
libsdl2-2.0-0 libsdl2-dev

sudo apt-get install libcdio-dev libsdl1.2-dev libsdl-net1.2-dev libsdl-net1.2-dev zlib1g-dev gcc g++ make cpp binutils

./configure
./configure && make && sudo make install


Após compilado fica em /usr/local/bin/

adicionar full screen /home/dz/.mednafen/mednafen-09x.cfg

"aspect_mult2" para "full"

mednafen -video.driver sdl -video.fs 1 -video.frameskip 0 -pce_fast.xres 0 -pce_fast.yres 0 LOCALDOGAME


Alterar o mednafen.cfg
;Force interlaced video to be treated as progressive.
cdplay.shader.goat.fprog 1

;Full-screen vertical resolution.
cdplay.yres 1

CATEGORIAS
###########################
Ação
Corrida
Luta
Aventura
Esporte

###########################
SEGA

Comics Zone - Ação
Kid Chameleon - Aventura
Sonic The Hedgehog 3 - Aventura
Vectorman 2 - Aventura
Vectorman - Aventura
Alladin - Aventura
Donkey Kong 99 - Aventura
Mega Bomberman - Aventura
Mega Bomberman - Aventura
Moonwalker Michael - Aventura
Streets Of Range 3 - Ação
Streets Of Range 2 - Ação
X-Mem 2 - Ação
Mortal Kombat 3 - Luta
Mortal Kombat 5 - Luta
Dragon Bruce Lee - Luta
Shinobi 3 - Ação
Super Hang On - Corrida
Kawasaki Superbike - Corrida
Road Rash 3 - Corrida
Road Rash 2 - Corrida
Top Gear 2 - Corrida
Super Monaco GP - Corrida
Out Run - Corrida
Golden Axe III - Ação
Golden Axe II - Ação
Shaq Fu - Ação
Robocop Vs Terminator - Ação
Mutant Ninja Turtles - Ação
Altered Beast - Ação
Side Pocket - Esporte

###########################
Super Nintendo

Super Mario World - Aventura
Donkey Kong - Aventura
Super Mario Kart - Corrida
Ultimate Mortal Kombat - Luta
Contra III - Aventura
Final Fight 3 - Ação
X-Men Mutant Apocalypse - Ação
Marvel Super Heroes - Ação
Side Pocket - Esporte
Mickey Mania - Aventura
Street Fighter - Luta
Mega Man 7 - Aventura
Spider-Man Separation - Ação
Doom Troopers - Ação
Legend Of Zelda - Aventura
Sonic Blast Man 2- Ação
Ms. Pac Man - Aventura
The Mask - Aventura
Super Soccer - Esporte
